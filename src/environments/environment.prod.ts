export const environment = {
  production: true,
  backend: {
    baseURL: 'https://tomsportfolioapi.azurewebsites.net',
  },
};
