export interface SiteStatistics {
  date: Date;
  totalVisits: number;
}
