import { Component } from '@angular/core';
import { LinkComponent } from '../link/link.component';
import { TextSectionComponent } from '../text-section/text-section.component';

@Component({
  selector: 'app-projects-card',
  standalone: true,
  imports: [LinkComponent, TextSectionComponent],
  templateUrl: './projects-card.component.html',
  styleUrls: ['./projects-card.component.scss'],
})
export class ProjectsCardComponent {}
