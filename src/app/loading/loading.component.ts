import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading',
  standalone: true,
  imports: [],
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
})
export class LoadingComponent implements OnInit, OnDestroy {
  dots = '';
  loading = true;
  loadingIntervalMs = 300;

  ngOnInit(): void {
    this.load();
  }

  ngOnDestroy(): void {
    this.loading = false;
  }

  load(): void {
    setTimeout(() => {
      if (this.dots.length < 3) {
        this.dots += '.';
      } else {
        this.dots = '';
      }
      if (this.loading) {
        this.load();
      }
    }, this.loadingIntervalMs);
  }
}
