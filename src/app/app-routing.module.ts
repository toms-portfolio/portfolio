import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AboutCardComponent } from './about-card/about-card.component';
import { TitleCardComponent } from './title-card/title-card.component';
import { ProjectsCardComponent } from './projects-card/projects-card.component';
import { RobotTableComponent } from './toy-robot-assessment/components/table/robot-table.component';

const routes: Routes = [
  { path: '', component: TitleCardComponent },
  {
    path: 'about',
    data: { preload: true },
    component: AboutCardComponent,
  },
  {
    path: 'projects',
    data: { preload: true },
    component: ProjectsCardComponent,
  },
  {
    path: 'toy-robot',
    data: { preload: true },
    component: RobotTableComponent,
  },
  { path: '**', redirectTo: '/' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
