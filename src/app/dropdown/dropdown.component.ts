import { Component, Input, OnInit, inject } from '@angular/core';
import {
  NgbDropdownConfig,
  NgbDropdownModule,
} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { NavigationItem } from './navigation-item';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-dropdown',
  standalone: true,
  imports: [NgbDropdownModule, CommonModule],
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  providers: [NgbDropdownConfig],
})
export class DropdownComponent implements OnInit {
  router = inject(Router);
  @Input() icon = '';

  constructor(config: NgbDropdownConfig) {
    config.placement = 'top-start';
    config.autoClose = this.isMobile() ? true : 'outside';
  }

  public buttons: NavigationItem[] = [
    { name: 'Home', url: '', selected: true },
    { name: 'About Me', url: 'about', selected: false },
    { name: 'My Projects', url: 'projects', selected: false },
    { name: 'Toy Robot', url: 'toy-robot', selected: false },
    { name: 'View Resume', url: 'resume', selected: false },
  ];

  ngOnInit(): void {
    // On navigate, hide buttons matching the current url
    this.router.events.subscribe(() => {
      this.buttons.forEach((btn) => {
        btn.selected = this.router.url === `/${btn.url}`;
      });
    });
    this.router.events.subscribe(() => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      window.scrollTo({ top: 0, behavior: 'instant' });
    });
  }

  navigate(url: string): void {
    if (url === 'resume') {
      window.open("/assets/documents/Tom%20Hunter's%20Resume.pdf", '_blank');
      return;
    }
    this.router.navigateByUrl(url);
  }

  isMobile(): boolean {
    return window.innerWidth <= 800;
  }
}
