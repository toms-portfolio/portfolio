import { TestBed } from '@angular/core/testing';

import { RobotCommandService } from './robot-command.service';

describe('RobotServiceService', () => {
  let service: RobotCommandService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RobotCommandService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
