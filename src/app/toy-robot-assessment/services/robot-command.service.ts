import { Injectable } from '@angular/core';
import { Command } from '../enums/command';
import { RobotInstructions } from '../interfaces/robot-instructions';
import { CardinalDirection } from '../enums/cardinal-direction';
import { RobotPosition } from '../interfaces/robot-position';

@Injectable({
  providedIn: 'root',
})
export class RobotCommandService {
  placeRegex = new RegExp(`([0-9])\,([0-9])\,(NORTH|EAST|SOUTH|WEST)`);
  instructions: RobotInstructions[] = [];
  command = '';
  maxTableDimensions = 4;
  usedPlace = false;

  /**
   * Process commands for the robot.
   *
   * Accepted Commands:
   * PLACE X,Y,F will place the Toy Robot on the table at position X,Y and facing direction F.
   * Direction can be one of the cardinal points: NORTH, EAST, SOUTH or WEST.
   * MOVE will move the Toy Robot one unit forward in the direction it is currently facing.
   * LEFT will rotate the Toy Robot 90 degrees left (anti-clockwise/counter-clockwise).
   * RIGHT will rotate the Toy Robot 90 degrees right (clockwise).
   * REPORT will announce the X,Y,F of Toy Robot.
   * @param commandInput
   * @returns instructions for robot or NULL (failed)
   */
  processCommand(commandInput: string): RobotInstructions[] | null {
    if (!commandInput) return null;
    this.command = commandInput.toLocaleUpperCase();

    const commands = this.command.split(/\s+/).filter((cmd) => cmd);
    commands.forEach((cmd, index) => {
      if (cmd == Command.PLACE && commands[index + 1]) {
        this.processPlace(commands[index + 1]);
        this.usedPlace = true;
      }

      // Only process commands if contains place (as per constraint)
      if (!this.usedPlace) return;
      this.processOther(cmd);
    });

    return this.instructions?.length ? this.instructions : null;
  }

  processPlace(positionInput: string): void {
    var placePositionRegex: RegExpExecArray | null =
      this.placeRegex.exec(positionInput);
    if (placePositionRegex) {
      const position: RobotPosition = {
        x: Number(placePositionRegex[2]),
        y: Number(placePositionRegex[1]),
        f: placePositionRegex[3] as CardinalDirection,
      };

      this.command = this.command.replace(placePositionRegex[0], '');

      this.instructions.push({
        command: Command.PLACE,
        position,
      });
    }
  }

  // Process MOVE, LEFT, RIGHT, and REPORT
  processOther(commandInput: string): void {
    // Process non-place commands
    Object.keys(Command).forEach((command) => {
      if (!commandInput.includes(command) || command == Command.PLACE) return;

      this.command = this.command.replace(command, '');
      this.instructions.push({
        command: command as Command,
      });
    });
  }

  reset(): void {
    this.command = '';
    this.instructions = [];
  }

  getMoveDirection(
    currentPosition: RobotPosition,
    command: Command
  ): RobotPosition {
    const directions = [
      CardinalDirection.NORTH,
      CardinalDirection.EAST,
      CardinalDirection.SOUTH,
      CardinalDirection.WEST,
    ];
    const totalDirections = directions.length;
    const currentDirection = currentPosition.f;
    let directionIndex = directions.indexOf(currentDirection);
    let newPosition = { ...currentPosition };

    switch (command) {
      case Command.MOVE:
        newPosition = this.getForwardMovement(
          currentPosition,
          directions[directionIndex]
        );
        break;
      case Command.LEFT:
        directionIndex -= 1;
        if (directionIndex < 0) directionIndex = directions.length - 1;
        break;
      case Command.RIGHT:
        directionIndex += 1;
        if (directionIndex > totalDirections - 1) directionIndex = 0;
        break;
    }
    newPosition.f = directions[directionIndex];
    return newPosition;
  }

  // Get new position one unit in front of where the robot is facing and located
  private getForwardMovement(
    currentPosition: RobotPosition,
    newDirection: CardinalDirection
  ): RobotPosition {
    const newPosition = { ...currentPosition };
    const maxSize = this.maxTableDimensions;
    switch (newDirection) {
      case CardinalDirection.NORTH:
        newPosition.x += 1;
        if (newPosition.x > maxSize) newPosition.x = maxSize;
        break;
      case CardinalDirection.EAST:
        newPosition.y -= 1;
        if (newPosition.y < 0) newPosition.y = 0;
        break;
      case CardinalDirection.SOUTH:
        newPosition.x -= 1;
        if (newPosition.x < 0) newPosition.x = 0;
        break;
      case CardinalDirection.WEST:
        newPosition.y += 1;
        if (newPosition.y > maxSize) newPosition.y = maxSize;
        break;
    }
    return newPosition;
  }
}
