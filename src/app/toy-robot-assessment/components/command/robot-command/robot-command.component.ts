import {
  Component,
  EventEmitter,
  inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { Command } from 'src/app/toy-robot-assessment/enums/command';
import { RobotPosition } from 'src/app/toy-robot-assessment/interfaces/robot-position';
import { RobotCommandService } from 'src/app/toy-robot-assessment/services/robot-command.service';

@Component({
  selector: 'app-robot-command',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './robot-command.component.html',
  styleUrls: ['./robot-command.component.scss'],
})
export class RobotCommandComponent implements OnInit {
  @Input() report = '';
  @Input() error = '';
  @Input() robotPosition: RobotPosition;
  @Output() moveRobot = new EventEmitter<RobotPosition>();
  formBuilder = inject(FormBuilder);
  robotCommandService = inject(RobotCommandService);
  previousPosition: RobotPosition;

  commandForm = this.formBuilder.group({
    command: '',
  });

  ngOnInit(): void {
    this.previousPosition = { ...this.robotPosition };
  }

  submitCommand(): void {
    const command = this.commandForm.value?.command ?? '';
    const robotInstructions = this.robotCommandService.processCommand(command);
    if (!robotInstructions) return this.onInvalidInput();

    robotInstructions.forEach((instruction, index) => {
      switch (instruction.command) {
        case Command.PLACE:
          break;
        case Command.MOVE:
        case Command.LEFT:
        case Command.RIGHT:
          instruction.position = this.robotCommandService.getMoveDirection(
            { ...this.previousPosition },
            instruction.command
          );
          break;
        case Command.REPORT:
          instruction.report = true;
      }

      // Move according to position
      const position = instruction.position;
      let reportPosition = this.previousPosition;
      if (position) {
        this.error = '';
        this.previousPosition = { ...position };

        // Wait before performing each movement
        setTimeout(() => {
          this.moveRobot.emit(position);
          reportPosition = this.previousPosition;

          // Send report
          if (instruction.report)
            this.report = `${position.y},${position.x},${position.f}`;
        }, index * 500);
      } // Send report even if there is no position
      else if (instruction.report) {
        setTimeout(() => {
          this.report = `${reportPosition.y},${reportPosition.x},${reportPosition.f}`;
        }, index * 500);
      }
    });

    this.robotCommandService.reset();
  }

  private onInvalidInput(): void {
    this.error = 'Invalid Input. Please try again.';
    this.robotCommandService.reset();
  }
}
