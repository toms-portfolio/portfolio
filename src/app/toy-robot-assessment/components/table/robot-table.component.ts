import { Component, OnInit } from '@angular/core';
import { TableTile } from '../../interfaces/table-tile';
import { CommonModule } from '@angular/common';
import { RobotComponent } from '../robot/robot.component';
import { RobotPosition } from '../../interfaces/robot-position';
import { CardinalDirection } from '../../enums/cardinal-direction';
import { RobotCommandComponent } from '../command/robot-command/robot-command.component';
import { LinkComponent } from '../../../link/link.component';

@Component({
  selector: 'app-toy-robot',
  standalone: true,
  imports: [
    CommonModule,
    RobotComponent,
    RobotCommandComponent,
    LinkComponent,
    LinkComponent,
  ],
  templateUrl: './robot-table.component.html',
  styleUrls: ['./robot-table.component.scss'],
})
export class RobotTableComponent implements OnInit {
  tiles: TableTile[] = [];
  dimension = 5;
  robotPosition: RobotPosition = {
    x: 2,
    y: 2,
    f: CardinalDirection.NORTH,
  };

  ngOnInit(): void {
    for (let x = this.dimension - 1; x >= 0; x--)
      for (let y = this.dimension - 1; y >= 0; y--) this.addTile({ x, y });
  }

  addTile(tile: TableTile): void {
    this.tiles.push(tile);
  }

  tileHasRobot(tile: TableTile): boolean {
    const tileHasRobot =
      tile.x == this.robotPosition.x && tile.y == this.robotPosition.y;
    return tileHasRobot;
  }

  moveRobot(position: RobotPosition): void {
    this.robotPosition = position;
  }
}
