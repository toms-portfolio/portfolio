import { Component, Input } from '@angular/core';
import { CardinalDirection } from '../../enums/cardinal-direction';

@Component({
  selector: 'app-robot',
  standalone: true,
  imports: [],
  templateUrl: './robot.component.html',
  styleUrls: ['./robot.component.scss'],
})
export class RobotComponent {
  @Input() direction: CardinalDirection;

  rotateDirection(direction: CardinalDirection): string {
    switch (direction) {
      case CardinalDirection.EAST:
        return 'rotate(90deg)';
      case CardinalDirection.SOUTH:
        return 'rotate(180deg)';
      case CardinalDirection.WEST:
        return 'rotate(270deg)';
      default:
        return '';
    }
  }
}
