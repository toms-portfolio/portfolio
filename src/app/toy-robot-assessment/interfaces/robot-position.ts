import { CardinalDirection } from '../enums/cardinal-direction';

export interface RobotPosition {
  x: number;
  y: number;
  f: CardinalDirection;
}
