import { Command } from '../enums/command';
import { RobotPosition } from './robot-position';

export interface RobotInstructions {
  command?: Command;
  position?: RobotPosition;
  report?: boolean;
}
