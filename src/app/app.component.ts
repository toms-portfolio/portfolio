import { Component, inject, OnInit } from '@angular/core';
import { catchError, delay, map, Observable, of, throwError } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { ApiService } from './services/api.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = `Tom Hunter's Portfolio`;

  constructor(titleService: Title) {
    titleService.setTitle(this.title);
  }

  apiService = inject(ApiService);

  loading$: Observable<boolean>;
  loadingDelayMs = 0;

  ngOnInit(): void {
    this.loading$ = of(true).pipe(delay(this.loadingDelayMs));

    console.log('Testing API...');
    this.apiService.getApiConnected().subscribe(() => {
      console.log('Api works!');
    });
  }
}
