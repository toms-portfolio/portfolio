import { inject, Injectable } from '@angular/core';
import { SiteStatistics } from '../interfaces/site-statistics';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  http = inject(HttpClient);
  baseUrl = environment.backend.baseURL;

  getApiConnected(): Observable<void> {
    return this.http.get<any>(this.baseUrl).pipe(catchError(this.handleError));
  }

  getSiteStatistics(): Observable<SiteStatistics> {
    return this.http
      .get<SiteStatistics>(this.baseUrl)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    console.error(error);
    return throwError(() => 'An API error has occurred.');
  }
}
